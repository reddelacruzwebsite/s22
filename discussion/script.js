"use strict";

// alert("Hello Batch 197!")


// ARRAY METHODS

/*
	Mutator Methods
		- seeks to modify the contents of an array.
		- are function that mutate an array after they are created. These methods manipulate orignal array performing various tasks such as adding or removing elements.
	
*/

let fruits = ["Apple", "Orange", "Kiwi", "Watermelon"]

/*
	push()
	 	- adds an element in the end of an array and returns the array's length

	Syntax:
		arrayName.push(element)

*/

console.log("Current Fruits Array:")
console.log(fruits)

// Adding element/s

fruits.push("Mango")
console.log(fruits)
let fruitsLength = fruits.push("Melon")
console.log(fruitsLength)
console.log(fruits)

fruits.push("Avocado", "Guava")
console.log(fruits)


/*
	pop()
		- removed the last element in our array and returns the removed element (when applied inside a varaible)

	Syntax:
		arrayName.pop()
*/


let removedFruit = fruits.pop()
console.log(removedFruit)
console.log("Mutated array from the pop method:")
console.log(fruits)


/*
	unshift()

		- adds one or more elements at the beginning of an array
		- returns the length of the array (when presented inside a variable)

	Syntax:
		arrayName.unshift(elementA)
		arrayName.unshift(elementA, elementB)

*/

fruits.unshift("Lime", "Banana")
console.log("Mutated array from the unshift method:")
console.log(fruits)


/*
	shift()
		- removes an element at the beginning of our array and return the removed element

	Syntax:
		arrayName.shift()

*/

let removedFruit2 = fruits.shift()
console.log(removedFruit2)
console.log("Mutated array from the shift method:")
console.log(fruits)

/*
	splice()
		- allows to simultaneously remove elements from a specified index number and adds an element

	Syntax:
		arrayName.splice(stardingIndex, deleteCount, elementsToBeAdded)

*/

let fruitsSplice = fruits.splice(1, 2, "Cherry", "Lychee")
console.log("Mutated arrays from splice method:")
console.log(fruits)
console.log(fruitsSplice)

// Using splice but without adding elements
let removedSplice = fruits.splice(3, 2)
console.log(fruits)
console.log(removedSplice)

/*
	sort()
		- reannges the array element in alphanumeric order

	Syntax:
		arrayName.sort()

*/

fruits.sort()
console.log("Mutated array from sort method:")
console.log(fruits)


let mixedArr = [12, "May", 36, 94, "August", 5, 6.3, "September", 10, 100, 1000]
console.log(mixedArr.sort())


/*
	reverse()
		- reverses the order or the element in an array

	Syntax:
		arrayName.reverse();
*/


fruits.reverse()
console.log("Mutated array from reverse method:")
console.log(fruits)

// For sorting the items in descending order:

	fruits.sort().reverse()
	console.log(fruits)


	/*MINI ACTIVITY #1:

	 - Debug the function which will allow us to list fruits in the fruits array.
	 	-- this function should be able to receive a string.
	 	-- determine if the input fruit name already exist in the fruits array.
	 		*** If it does, show an alert message: "Fruit already listed on our inventory".
	 		*** If not, add the new fruit into the fruits array ans show an alert message: "Fruit is not listed in our inventory."
	 	-- invoke and register a new fruit in the fruit array.
	 	-- log the updated fruits array in the console


	 	function registerFruit () {
	 		let doesFruitExist = fruits.includes(fruitName);

	 		if(doesFruitExst) {
	 			alerat(fruitName "is already on our inventory")
	 		} else {
	 			fruits.push(fruitName);
	 			break;
	 			alert("Fruit is now listed in our inventory")
	 		}
	 	}
	 	
	*/

// const newFruits = ["banana", "pomelo", "durian", "mango", "kiwi"]

// function registerFruit (fruitName) {
// 	 		let doesFruitExist = newFruits.includes(fruitName);

// 	 		if(doesFruitExist) {
// 	 			alert(fruitName + " is already on our inventory")
// 	 		} else {
// 	 			newFruits.push(fruitName);
// 	 			alert(fruitName + " is now listed in our inventory")
// 	 		}
// 	 	}

// registerFruit("cherry");

// console.log(newFruits);


	/*MINI ACTIVITY #2:*/

	const avengers = [];

avengers.push("Wanda","Thor","Captain America", "Hulk", "Spiderman", "Black Widow", "Black Panther", "Hawkeye", "Thanos", "Vision");
console.log(avengers);

avengers.unshift("Ironman");
console.log(avengers);

avengers.pop();
console.log(avengers);

avengers.shift();
console.log(avengers);


avengers.sort();
console.log(avengers);













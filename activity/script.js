"use strict";

/*
    Create functions which can manipulate our arrays.
    You may change the names on the initial list before you use the array varaibles.
*/

let registeredUsers = [

    "James Jeffries",
    "Maggie Williams",
    "Macie West",
    "Michelle Queen",
    "Angelica Smith",
    "Fernando Dela Cruz",
    "Mike Dy"
];

let friendsList = [];

/*
    
   1. Create a function which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, show an alert window with the following message:
                "Registration failed. Username already exists!"
            -if it is not, add the new username into the registeredUsers array and show an alert:
                "Thank you for registering!"
        - Invoke and register a new user in the browser console.
        - In the browser console, log the registeredUsers array.

*/
    

    const registerAUser = function (newUser) {
    	const userExists = registeredUsers.includes(newUser);

	 		if(userExists) {
	 			alert("Registration failed. Username already exists!")
	 		} else {
	 			registeredUsers.push(newUser);
	 			alert("Thank you for registering!")
	 		}
    }
	registerAUser("Red Dela Cruz");
	console.log(registeredUsers);


/*
    2. Create a function which will allow us to add a registered user into our friends list.
        - This function should be able to receive a string.
        - Determine if the input username exists in our registeredUsers array.
            - If it is, add the foundUser in our friendList array.
                    -Then show an alert with the following message:
                        - "You have added <registeredUser> as a friend!"
            - If it is not, show an alert window with the following message:
                - "User not found."
        - Invoke the function and add a registered user in your friendsList in the browser console.
        - In the browser console, log the friendsList array in the console.

*/

const addToFriendsList = function (newFriend) {

	const isRegistered = registeredUsers.includes(newFriend);

	if(isRegistered) {
		friendsList.push(newFriend);
		alert(`You have added ${newFriend} as a friend!`);
		} else {
		alert(`User not found.`);
		}
	}
	addToFriendsList("Red Dela Cruz");
	addToFriendsList("Macie West");
	addToFriendsList("Michelle Queen");
	addToFriendsList("Mike Dy");
	console.log(friendsList);


/*
    3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty show an alert: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function.

*/

const showFriendsList = function () {
	console.log("Your friends list:");
	friendsList.forEach(friend => console.log(friend));
}
showFriendsList();

    
// STRETCH GOALS

/*
    4. Create a function which will display the amount of registered users in your friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty show an alert:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function in the browser console.
*/

const showNumberOfFriends = function () {
	const hasFriends = friendsList.length > 0;

	if (hasFriends) {
		alert(`You currently have ${friendsList.length} friends.`);
	} else {
		alert(`You currently have 0 friends. Add one first.`);
	}
}
showNumberOfFriends();

/*
    5. Create a function which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - In the browser console, log the friendsList array.

*/

const deleteLastFriend = function () {
	const hasFriends = friendsList.length > 0;

	if (hasFriends) {
		friendsList.pop();
	} else {
		alert(`You currently have 0 friends. Add one first.`);
	}
}
deleteLastFriend();
console.log(friendsList);

/*======================================================================================*/

// Try this for fun:
/*
    Instead of only deleting the last registered user in the friendsList delete a specific user instead.
        -You may get the user's index.
        -Then delete the specific user with splice().
*/


const deleteAFriend = function () {
	friendsList.splice(1,1);
}

deleteAFriend();
console.log(friendsList);

